import React from 'react'
import '../styles/product.scss'

export default function Product() {
    return (
        <div className="header-product-container">
            <div className="row">
                <div className="col-7">
                    <div className="product-title-span">
                        <h1 className="product-title">Product Name</h1>
                    </div>
                    <div className="product-title-li text-light pt-4">
                        <i className="fas fa-check"/> Put on this page information about your product <br/>
                        <i className="fas fa-check"/> A detailed description of your product <br/>
                        <i className="fas fa-check"/> Tell us about the advantages and merits <br/>
                        <i className="fas fa-check"/> Associate the page with the payment system <br/>
                    </div>
                </div>
                <div className="col-5">
                    <div className="right-product-cont bg-white" />
                </div>
            </div>
        </div>

    )
}