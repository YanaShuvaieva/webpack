import React from 'react'
import '../styles/about.scss'

export default function About() {
    return (
        <div className="about-product-container">
            <div className="row">
                <div className="col-8">
                    <div className="about-cont-prod">
                        <h2 className="product-about">About your product</h2>
                        <div className="product-about-text pt-3">
                            <p className="text-about">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </p>
                            <p className="text-about">
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                                nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                deserunt mollit anim id est laborum.
                            </p>
                        </div>
                    </div>
                </div>
                <div className="col-4">
                    <div className="right-about-text" />
                </div>
            </div>
        </div>
    )
}