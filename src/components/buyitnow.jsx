import React from 'react'
import '../styles/buyitnow.scss'

export default function Buyitnow() {
    return(
        <div className="block-buy">
            <h2 className="text-center mb-4">Buy it now</h2>
            <div className="row">
                <div className="position-relative col text-light p-0 block-buy__product mr-3 ml-3">
                    <h3 className="text-center m-3">Standart</h3>
                    <div className="bg-white text-center">
                        <p className="block-buy__product_price">
                            $100
                        </p>
                    </div>
                    <ol className="text-light">
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                    </ol>
                    <input type="button" className="text-center bg-white block-buy__product_btn" value="BUY"/>
                </div>
                <div className="position-relative col text-light p-0 block-buy__product mr-3 ml-3">
                    <h3 className="text-center m-3">Premium</h3>
                    <div className="bg-white text-center">
                        <p className="block-buy__product_price">
                            $150
                        </p>
                    </div>
                    <ol className="text-light">
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                    </ol>
                    <input type="button" className="text-center bg-white block-buy__product_btn" value="BUY"/>
                </div>
                <div className="position-relative col text-light p-0 block-buy__product mr-3 ml-3">
                    <h3 className="text-center m-3">Lux</h3>
                    <div className="bg-white text-center">
                        <p className="block-buy__product_price">
                            $200
                        </p>
                    </div>
                    <ol className="text-light">
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                        <li className="m-1">Lorem ipsum dolor sit amet</li>
                    </ol>
                    <input type="button" className="text-center bg-white block-buy__product_btn" value="BUY"/>
                </div>
            </div>

        </div>
    )
}