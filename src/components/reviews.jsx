import React from 'react';
import '../styles/reviews.scss'

export default function Reviews() {
    return (
        <div className='reviews-wrap-block'>
            <h2 className="reviews-title text-center">Reviews</h2>
            <div className="reviews-block">
                <div className="row pt-4">
                    <div className="row col">
                        <div className="col-md-auto mr-4 ml-4 img-reviews-block"/>
                        <div className="col reviews_message">
                            <p className="font-italic"> Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                            </p>
                            <p className="text-muted">Lourens S.</p>
                        </div>
                    </div>
                    <div className="row col">
                        <div className="col-md-auto mr-4 ml-4 img-reviews-block"/>
                        <div className="col reviews_message">
                            <p className="font-italic"> Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                            </p>
                            <p className="text-muted">Lourens S.</p>
                        </div>
                    </div>
                </div>
                <div className="row pt-4">
                    <div className="row col">
                        <div className="col-md-auto mr-4 ml-4 img-reviews-block"/>
                        <div className="col reviews_message">
                            <p className="font-italic"> Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                            </p>
                            <p className="text-muted">Lourens S.</p>
                        </div>
                    </div>
                    <div className="row col">
                        <div className="col-md-auto mr-4 ml-4 img-reviews-block"/>
                        <div className="col reviews_message">
                            <p className="font-italic"> Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                            </p>
                            <p className="text-muted">Lourens S.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }