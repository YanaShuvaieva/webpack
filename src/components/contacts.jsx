import React from 'react'
import '../styles/contacts.scss'

export default function Contact() {
    return (
        <div className="contact-container">
            <h2 className="product-about text-center">Contacts</h2>
            <div className="row">
                <div className="col-7 contact-form-form">
                    <form>
                        <input className="mb-2 input-product" type="text" placeholder="Your name: "/>
                        <input className="mb-2 input-product" type="text" placeholder="Your email: "/>
                        <textarea className="mb-2 input-product textarea-inp" placeholder="Your message: "/>
                        <div className="d-flex justify-content-center">
                            <input className="btn-block-product" type="submit" value="Send"/>
                        </div>
                    </form>
                </div>
                <div className="col-5 logo-con">
                    <p><i className="fab fa-skype mr-1"/>here_your_login_skype</p>
                    <p><i className="fab fa-telegram-plane mr-1"/>here_your_telegram</p>
                    <p><i className="fas fa-at mr-1"/>yankashuvaeva1@gmail.com</p>
                    <p><i className="fas fa-phone mr-1"/>+38-(050)-888-8888</p>

                        <div className="d-flex mt-4 logo-contact">
                            <i className="fab fa-twitter"/>
                            <i className="fab fa-facebook-f"/>
                            <i className="fab fa-linkedin-in"/>
                            <i className="fab fa-google"/>
                            <i className="fab fa-youtube"/>
                        </div>
                </div>
            </div>
        </div>
    )
}