import React from 'react';
import '../styles/screenshots.scss'


export default function Screenshots() {
    return (
        <div className='screenshots-block'>
            <h2 className="text-center mb-4">Screenshots</h2>
            <div className="row">
                <div className="col row p-0">
                    <div className="col-4 p-0 scr-img-block"/>
                        <div className="col-8">
                            <h3>The description for the image</h3>
                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                            </p>
                        </div>
                </div>
                <div className="col row p-0">
                    <div className="col-4 p-0 scr-img-block"/>
                    <div className="col-8">
                        <h3>The description for the image</h3>
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                        </p>
                    </div>
                </div>
            </div>
            <div className="row pt-4">
                <div className="col row p-0">
                    <div className="col-4 p-0 scr-img-block"/>
                    <div className="col-8">
                        <h3>The description for the image</h3>
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                        </p>
                    </div>
                </div>
                <div className="col row p-0">
                    <div className="col-4 p-0 scr-img-block"/>
                    <div className="col-8">
                        <h3>The description for the image</h3>
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}