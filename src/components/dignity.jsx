import React from 'react'
import '../styles/dignity.scss'

export default function Dignity() {
    return(
        <section className="section-pluses">
            <h2 className="text-center mb-4">Dignity and pluses product</h2>
            <div className="row pt-2">
                <div className="col d-flex">
                    <i className="fas fa-plus-square mr-2"></i>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
                <div className="col d-flex">
                    <i className="fas fa-plus-square mr-2"></i>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
            </div>
            <div className="row">
                <div className="col d-flex">
                    <i className="fas fa-plus-square mr-2"></i>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
                <div className="col d-flex">
                    <i className="fas fa-plus-square mr-2"></i>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
            </div>
            <div className="row">
                <div className="col d-flex">
                    <i className="fas fa-plus-square mr-2"></i>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
                <div className="col d-flex">
                    <i className="fas fa-plus-square mr-2"></i>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
            </div>
        </section>
    );
}