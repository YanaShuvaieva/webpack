import React from 'react';

import Product from './components/product';
import About from './components/about';
import Dignity from './components/dignity';
import Screenshots from './components/screenshots';
import Reviews from './components/reviews';
import Buyitnow from './components/buyitnow'
import Contacts from './components/contacts';

export function App() {
    return (
        <div className='app-wrapper'>
            <Product/>
            <About/>
            <Dignity/>
            <Screenshots/>
            <Reviews/>
            <Buyitnow/>
            <Contacts/>
        </div>
    );
}

