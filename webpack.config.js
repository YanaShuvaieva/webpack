
const fs = require('fs');
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const isDev = process.env.NODE_ENV !== 'production';

function getName(ext) {
    return `[name].[${isDev ? 'hash:8' : 'contenthash'}].bundle.${ext}`
}

const ROOT_DIR = fs.realpathSync(process.cwd())

function pathResolve(...args) {
    return path.resolve(ROOT_DIR, ...args)
}

module.exports = {
    mode: isDev ? 'development' : 'production',
    entry: {
        main: './src/index.jsx'
    },
    output: {
        filename: getName('js'),
        path: pathResolve('dist')
    },
    resolve: {
        extensions: ['.js', '.json', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: [/node_modules/],
                use: ['babel-loader']
            },
            {
                test: /\.(css)$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.s[ac]ss$/i,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                    },
                ],
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/'
                        }
                    }
                ]
            }

        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: pathResolve('public/index.html')
        }),
        new MiniCssExtractPlugin()
    ],
    devServer: {
        port: '3000',
        open: true
    }
};